import React from 'react';
import ReactDOM from 'react-dom';
import PivotTableUI from 'react-pivottable/PivotTableUI';
import 'react-pivottable/pivottable.css';
import Books from "../../BooksComponent";
import TableRenderers from 'react-pivottable/TableRenderers';
import Plot from 'react-plotly.js';
import createPlotlyRenderers from 'react-pivottable/PlotlyRenderers';
import axios from "axios/index";
import { message, Spin } from 'antd';

// create Plotly renderers via dependency injection
const PlotlyRenderers = createPlotlyRenderers(Plot);



class TestTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: [],
            props,
        };
    }

    fetch = () => {
        this.setState({ loading: true });
        axios.get('http://10.10.10.33:8090/api/v1/potensi/tampilkanGl')
            .then((response) => {
                console.log(response.data);
                this.setState({
                    loading: false,
                    data: response.data,
                });
            })
            .catch(error => {
                this.setState({
                    data: []
                });
                message.error(`Status error code ${error.response.code}`);
                console.log(error.response.data);
            });
    }

    componentDidMount() {
        this.fetch();
    }

    render() {
        return (
            <Spin tip="Loading..." spinning={this.state.loading}>
                <PivotTableUI
                    data={this.state.data}
                    onChange={s => this.setState(...this.state, s)}
                    renderers={Object.assign({}, TableRenderers, PlotlyRenderers)}
                    {...this.state}
                />
            </Spin>

        );
    }
}

export default TestTable;