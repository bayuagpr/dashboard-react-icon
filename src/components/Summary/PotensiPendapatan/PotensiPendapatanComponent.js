import React from 'react';
import PivotTableGo from './tablepivotgo';
import PivotTableGl from './tablepivotgl';
import { Tabs } from 'antd';

const TabPane = Tabs.TabPane

function callback(key) {
    console.log(key);
}

class PotensiPendapatanComponent extends React.Component {

    render() {
        return (
            <div>
                <h1 style={{fontSize:20}}>Potensi Pendapatan</h1>
                <br/>
                <Tabs defaultActiveKey="1" onChange={callback}>
                    <TabPane tab="Potensi Group Owner" key="1">
                        <div style={{overflow: 'scroll', maxHeight: '440px', height: '440px'}}>
                        <PivotTableGo/>
                    </div>
                    </TabPane>
                    <TabPane tab="Potensi Group Layanan" key="2">
                        <div style={{overflow: 'scroll', maxHeight: '440px', height: '440px'}}>
                        <PivotTableGl/>
                    </div>
                    </TabPane>
                </Tabs>


            </div>
        );
    }
}
 export default PotensiPendapatanComponent;