import React from 'react';
import PivotTableAl from './tablepivotal';
import { Tabs } from 'antd';

const TabPane = Tabs.TabPane

function callback(key) {
    console.log(key);
}

class RealisasiPendapatanComponent extends React.Component {

    render() {
        return (
            <div>
                <h1 style={{fontSize:20}}>Realisasi Pendapatan</h1>
                <br/>
                <div style={{overflow: 'scroll', maxHeight: '440px', height: '440px'}}>
                    <PivotTableAl/>
                </div>


            </div>
        );
    }
}
 export default RealisasiPendapatanComponent;