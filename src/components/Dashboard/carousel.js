import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './carousel.css';
import { Carousel, message, Spin} from 'antd';
import axios from "axios/index";

export default class SlideDashboard extends React.Component {
    state = {
        data: [],
        loading: false,
    };
    fetch = () => {
        this.setState({ loading: true });
        axios.get('http://10.10.10.33:8090/api/v1/target/tampilkan')
            .then((response) => {
                console.log(response.data);
                this.setState({
                    loading: false,
                    data: response.data,
                });
            })
            .catch(error => {
                this.setState({
                    loading: false,
                    data: []
                });
                message.error(`Status error code ${error.response.code}`);
                console.log(error.response.data);
            });
    }

    componentDidMount() {
        this.fetch();
    }
    render() {
        const targets = this.state.data.map((value) => (
            <div>
                <h3></h3>
                <h3>{value.groupOwner}</h3>
                <h2 style={{color:'#fff', fontSize:50}}>{value.targetSbu}</h2>
            </div>
        ));
        return (
            <Spin tip="Loading..." spinning={this.state.loading}>
    <Carousel autoplay>
        {targets}
    </Carousel>
            </Spin>);
    }
}
