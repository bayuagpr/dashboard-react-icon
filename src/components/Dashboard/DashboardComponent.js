import React from 'react';
import Carousel from './carousel';
import TableDashboard from './table';

class DashboardComponent extends React.Component {
    render() {
        return (
            <div>
                <h1 style={{fontSize:20}}>Target SBU</h1><br/>
                <Carousel/>
                <div style={{ padding:10}}/>
                <h1 style={{fontSize:20}}>General Summary</h1><br/>
                <TableDashboard />
            </div>
        );
    }
}
 export default DashboardComponent;