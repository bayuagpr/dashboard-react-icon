import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { Table } from 'antd';
import {message} from "antd/lib/index";
import axios from "axios/index";

const columns = [{
  title: 'Group Owner',
  dataIndex: 'groupOwner',
  key: 'groupOwner'}, {
    title: 'Target',
    dataIndex: 'target',
    key: 'target'}, {
    title: 'CO NR',
    dataIndex: 'coNr',
    key: 'coNr'},
    {
        title: 'Potensi SO Closed',
        dataIndex: 'potensiSoClosed',
        key: 'potensiSoClosed'}, {
        title: 'Potensi SO Progress',
        dataIndex: 'potensiSoProgress',
        key: 'potensiSoProgress'}, {
        title: 'Potensi SO Open',
        dataIndex: 'potensiSoOpen',
        key: 'potensiSoOpen'},
    {
        title: 'Total',
        dataIndex: 'total',
        key: 'total'}, {
        title: 'Percent Target',
        dataIndex: 'persenTarget',
        key: 'persenTarget'}];



function onChange(pagination, filters, sorter) {
  console.log('params', pagination, filters, sorter);
}

export default class TableDashboard extends React.Component {
    state = {
        data: [],
        loading: false,
    };
    fetch = () => {
        this.setState({ loading: true });
        axios.get('http://10.10.10.33:8090/api/v1/dashboard/tablelDashboard')
            .then((response) => {
                console.log(response.data);
                this.setState({
                    loading: false,
                    data: response.data,
                });
            })
            .catch(error => {
                this.setState({
                    loading: false,
                    data: []
                });
                message.error(`Status error code ${error.response.code}`);
                console.log(error.response.data);
            });
    }

    componentDidMount() {
        this.fetch();
    }
    render() {
            return (
                <Table columns={columns} dataSource={this.state.data} onChange={onChange} pagination={false} loading={this.state.loading}/>);
        }
    }
