import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './upload.css';
import { Upload, Button, Icon, message, notification  } from 'antd';
import reqwest from 'reqwest';
import axios from 'axios'

export default class UploadSelect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fileList: [],
            uploading: false
        };
    }


  handleUpload = () => {
    const { fileList } = this.state;
    const formData = new FormData();
    fileList.forEach((file) => {
      formData.append('file', file);
    });

    this.setState({
      uploading: true,
    });

      axios.post( this.props.type===1 ? 'http://10.10.10.33:8090/api/v1/upload/alUpload':'http://10.10.10.33:8090/api/v1/upload/arUpload',
          formData,
          {
              headers: {
                  'Content-Type': 'multipart/form-data'
              }
          }
      ).then(() => {
          this.setState({
              fileList: [],
              uploading: false,
          });

      })
          .catch(error => {
              this.setState({
                  uploading: false,
              });
              message.error(`Status error code ${error.response.code}`);
              console.log(error.response.data);
          });

      }


  render() {
    const { uploading } = this.state;
     // const type = this.props.type;
    const props = {
        accept: '.xls,.xlsx',
        name: 'file',
      action: this.props.type===1 ? 'http://10.10.10.33:8090/api/v1/upload/alUpload':'http://10.10.10.33:8090/api/v1/upload/arUpload',
      onRemove: (file) => {
        this.setState(({ fileList }) => {
          const index = fileList.indexOf(file);
          const newFileList = fileList.slice();
          newFileList.splice(index, 1);
          return {
            fileList: newFileList,
          };
        });
      },
      beforeUpload: (file) => {
        this.setState(({ fileList }) => ({
          fileList: [...fileList, file],
        }));
        return false;
      },
        onChange(info) {
            if (info.file.status === 'done') {
                notification.success({
                    message: 'Upload Success!',
                    description: `${info.file.name} has been successfully uploaded`,
                });
            } else if (info.file.status === 'error') {
                notification.error({
                    message: 'Upload Failed!',
                    description: `${info.file.name} can't be processed`,
                });
            }
        },
      fileList: this.state.fileList,
        multipart:true,
        multiple: false
    };

    return (
      <div>
        <Upload {...props}>
          <Button style={{marginTop:'16px'}}>
            <Icon type="upload" /> Select File
          </Button>
        </Upload>
        <Button
          className="upload-demo-start"
          type="primary"
          onClick={this.handleUpload}
          disabled={this.state.fileList.length === 0}
          loading={uploading}
        >
          {uploading ? 'Uploading' : 'Start Upload' }
        </Button>
      </div>
    );
  }
}

          