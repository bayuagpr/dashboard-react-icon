import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import UploadSelect from './upload';
import { Radio } from 'antd';

const RadioGroup = Radio.Group;

export default class RadioButton extends React.Component {
  state = {
    value: 1,
  }

  onChange = (e) => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,

    })
    ;
  }



  render() {
    return (
        <div><RadioGroup onChange={this.onChange} value={this.state.value}>
            <Radio value={1}>Activation List</Radio>
            <Radio value={2}>Activation Request</Radio>
        </RadioGroup>
            <UploadSelect type={this.state.value}/>
        </div>

    );
  }
}

          