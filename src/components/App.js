import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import '../styles/App.css';
import logo from './icon+.png';
import { Layout, Menu, Icon } from 'antd';

import DashboardComponent from './Dashboard/DashboardComponent';
import ActivationListComponent from './DataCRM/ActivationList/ActivationListComponent';
import ActivationRequestComponent from './DataCRM/ActivationRequest/ActivationRequestComponent';
import GroupOwnerComponent from './DataCRM/GroupOwner/GroupOwnerComponent';
import GroupLayananComponent from './DataCRM/GroupLayanan/GroupLayananComponent';
import TargetSbuComponent from './DataCRM/TargetSbu/TargetSbuComponent';
import PotensiPendapatan from './Summary/PotensiPendapatan/PotensiPendapatanComponent';
import RealisasiPendapatan from './Summary/RealisasiPendapatan/RealisasiPendapatanComponent';
import UploadComponent from './Upload/UploadComponent';


import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";

const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;

class App extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = (collapsed) => {
    console.log(collapsed);
    this.setState({ collapsed });
  }

  render() {
    return (
      <Router>
        <Layout style={{ minHeight: '100vh' }}>
          <Header style={{ background: '#fff', padding: 0, position: 'fixed', zIndex: 4, width: '100%' }}>
            <img style={{margin:'0px 0px 0px 80px'}} src={logo} width="200" height="50"/>
          </Header>
          <Sider
            collapsible
            collapsed={this.state.collapsed}
            onCollapse={this.onCollapse}
          >
            <div />
            <Menu style={{ margin: '64px 0px 0px 0px', overflow: 'initial', maxWidth: '200px', height: '100vh', position: 'fixed', left: 0, zIndex: 1 }} theme="dark" mode="inline">
              <Menu.Item key="/">
                <Icon type="dashboard" />
                <span>Dashboard</span>
                <Link to="/" />
              </Menu.Item>
              <SubMenu
                key="/DataCRM"
                title={<span><Icon type="table" /><span>Data CRM</span></span>}
              >
                <Menu.Item key="/ActivationList">Activation List<Link to="/DataCRM/ActivationList" /></Menu.Item>
                <Menu.Item key="/ActivationRequest">Activation Request<Link to="/DataCRM/ActivationRequest" /></Menu.Item>
                <Menu.Item key="/GroupOwner">Group Owner<Link to="/DataCRM/GroupOwner" /></Menu.Item>
                <Menu.Item key="/GroupLayanan">Group Layanan<Link to="/DataCRM/GroupLayanan" /></Menu.Item>
                <Menu.Item key="/TargetSBU">Target SBU<Link to="/DataCRM/TargetSBU" /></Menu.Item>
              </SubMenu>  
              <SubMenu
                key="/Summary"
                title={<span><Icon type="profile" /><span>Summary</span></span>}
              >
                <Menu.Item key="/PotensiPendapatan">Potensi Pendapatan<Link to="/Summary/PotensiPendapatan" /></Menu.Item>
                <Menu.Item key="/RealisasiPendapatan">Realisasi Pendapatan<Link to="/Summary/RealisasiPendapatan" /></Menu.Item>
              </SubMenu>
              <Menu.Item key="/Upload">
                <Icon type="upload" />
                <span>Upload</span>
                <Link to="/Upload" />
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout>
            <Content style={{ margin: '80px 16px 16px 16px' }}>
              <div style={{ padding: 16, background: '#fff', overflow: 'initial' }}>
                <Switch>
                  <Route exact path="/" component={DashboardComponent} />
                  <Route path="/DataCRM/ActivationList" component={ActivationListComponent} />
                  <Route path="/DataCRM/ActivationRequest" component={ActivationRequestComponent} />
                  <Route path="/DataCRM/GroupOwner" component={GroupOwnerComponent} />
                  <Route path="/DataCRM/GroupLayanan" component={GroupLayananComponent} />
                  <Route path="/DataCRM/TargetSbu" component={TargetSbuComponent} />
                  <Route path="/Summary/PotensiPendapatan" component={PotensiPendapatan} />
                  <Route path="/Summary/RealisasiPendapatan" component={RealisasiPendapatan} />
                  <Route path="/Upload" component={UploadComponent} />
                </Switch>
              </div>
            </Content>
            <Footer style={{ maxHeight: '42px', textAlign: 'center', background: '#fff' }}>
              ©Copyright ICON+ 2018. All rights reserved.
          </Footer>
          </Layout>
        </Layout>
      </Router>
    );
  }
}

export default App;
