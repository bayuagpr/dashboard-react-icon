
import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './groupowner.css';
import { Table, Divider, Input, InputNumber, Popconfirm, Form, message } from 'antd';
import axios from "axios/index";


const FormItem = Form.Item;
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
        <tr {...props} />
    </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
    getInput = () => {
        if (this.props.inputType === 'number') {
            return <InputNumber />;
        }
        return <Input />;
    };

    render() {
        const {
            editing,
            dataIndex,
            title,
            inputType,
            record,
            index,
            ...restProps
        } = this.props;
        return (
            <EditableContext.Consumer>
                {(form) => {
                    const { getFieldDecorator } = form;
                    return (
                        <td {...restProps}>
                            {editing ? (
                                <FormItem style={{ margin: 0 }}>
                                    {getFieldDecorator(dataIndex, {
                                        rules: [{
                                            required: true,
                                            message: `Please Input ${title}!`,
                                        }],
                                        initialValue: record[dataIndex],
                                    })(this.getInput())}
                                </FormItem>
                            ) : restProps.children}
                        </td>
                    );
                }}
            </EditableContext.Consumer>
        );
    }
}

export default class TableGroupOwner extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
        data: [],
        loading: false,
        editingKey: '' };
    this.columns = [
        {
            title: 'Layanan',
            dataIndex: 'layanan',
            width: 200,
            editable: true,
        },
      {
        title: 'Group Layanan',
        dataIndex: 'groupLayanan',
        width: 200,
        editable: true,
      },
      {
        title: 'Jenis',
        dataIndex: 'jenis',
        width: 200,
        editable: true,
      },
      {
        title: 'operation',
        dataIndex: 'operation',
        render: (text, record) => {
          const editable = this.isEditing(record);
          return (
            <div>
              <span>
                <a>
                  {editable ? (
                    <span>
                      <EditableContext.Consumer>
                        {form => (
                          <a
                            href="javascript:;"
                            onClick={() => this.save(form, record.groupLayananId)}
                            style={{ marginRight: 8 }}
                          >
                            Save
                      </a>
                        )}
                      </EditableContext.Consumer>
                      <Popconfirm
                        title="Sure to cancel?"
                        onConfirm={() => this.cancel(record.groupLayananId)}
                      >
                        <a>Cancel</a>
                      </Popconfirm>
                    </span>
                  ) : (
                      <a onClick={() => this.edit(record.groupLayananId)}>Edit</a>
                    )}
                </a>
                <Divider type="vertical" />
                <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.groupLayananId)}>
                  <a href="javascript:;">Delete</a>
                </Popconfirm>
              </span>
            </div>
          );
        },
      },
    ];
  }

    fetch = () => {
        this.setState({ loading: true });
        axios.get('http://10.10.10.33:8090/api/v1/grLayanan/tampilkan')
            .then((response) => {
                console.log(response.data);
                this.setState({
                    loading: false,
                    data: response.data,
                });
            })
            .catch(error => {
                this.setState({
                    loading: false,
                    data: []
                });
                message.error(`Status error code ${error.response.code}`);
                console.log(error.response.data);
            });
    }

    componentDidMount() {
        this.fetch();
    }

  isEditing = (record) => {
    return record.groupLayananId === this.state.editingKey;
  };

  edit(groupLayananId) {
    this.setState({ editingKey: groupLayananId });
  }

    handleDelete = (groupLayananId) => {
        const dataSource = [...this.state.data];
        this.setState({ data: dataSource.filter(item => item.groupLayananId !== groupLayananId) });
    }


    save(form, groupLayananId) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const newData = [...this.state.data];
      const index = newData.findIndex(item => groupLayananId === item.groupLayananId);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        this.setState({ data: newData, editingKey: '' });
      } else {
        newData.push(row);
        this.setState({ data: newData, editingKey: '' });
      }
    });
  }

  cancel = () => {
    this.setState({ editingKey: '' });
  };

  render() {
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: 'text',
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });

    return (
      <Table
        components={components}
        bordered
        dataSource={this.state.data}
        columns={columns}
        rowClassName="editable-row"
        size="small"
        pagination={false}
        loading={this.state.loading}
        scroll={{ y: 336 }}
      />
    );
  }
}

