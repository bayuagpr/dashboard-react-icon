import React from 'react';
import TableGroupOwner from './groupowner';
import InsertGroupOwner from './insertgroupowner';

class GroupOwnerComponent extends React.Component {
    render() {
        return (
            <div>
                <h1 style={{fontSize:20}}>Group Owner</h1><br/>
                <InsertGroupOwner/>
                <br/>
                <TableGroupOwner />
            </div>
        );
    }
}
 export default GroupOwnerComponent;