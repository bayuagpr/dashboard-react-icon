
import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './tableAR.css';
import { Table, Input, Button, Icon , message } from 'antd';
import axios from 'axios';

export default class TableActivationRequest extends React.Component {
    state = {
        searchText: '',
        data: [],
        pagination: {},
        loading: false,
    };

    handleTableChange = (pagination) => {
        const pager = { ...this.state.pagination };
        pager.current = pagination.current;
        this.setState({
            pagination: pager,
        });

        this.fetch({
            page: pagination.current-1,
            size: pagination.pageSize,
        });
    }

    fetch = (param = {}) => {
        console.log('params:', param);
        this.setState({ loading: true });
        axios.get('http://10.10.10.33:8090/api/v1/view/rawAr/tampilkan', {
            params: {
                size: 50,
                ...param,
            }
        })
            .then((response) => {
                console.log(response.data);
                const pagination = { ...this.state.pagination };
                // Read total count from server
                pagination.total = response.data.totalElements;
                this.setState({
                    loading: false,
                    data: response.data.content,
                    pagination
                });
            })
            .catch(error => {
                this.setState({
                    loading: false,
                    data: []
                });
                message.error(`Status error code ${error.response.code}`);
                console.log(error.response.data);
            });
    }

    componentDidMount() {
        this.fetch();
    }

  handleSearch = (selectedKeys, confirm) => () => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  handleReset = clearFilters => () => {
    clearFilters();
    this.setState({ searchText: '' });
  }

  render() {
    const columns = [
    {title: 'Activation Request Number', width: 200, dataIndex: 'arNo', key: 'arNo', fixed: 'left'},
    { title: 'Activation Request Type', dataIndex: 'arType', key: 'arType', width: 150 },
    { title: 'Activation Request Status', dataIndex: 'arStatus', key: 'arStatus', width: 150 },
    { title: 'Activation Request Date', dataIndex: 'arDate', key: 'arDate', width: 150 },
    { title: 'Customer Name', dataIndex: 'custName', key: 'custName', width: 150 },
    { title: 'Segment', dataIndex: 'segment', key: 'segment', width: 150 },
    { title: 'Berita Acara Date', dataIndex: 'baDate', key: 'baDate', width: 150 },
    { title: 'Cancel Date', dataIndex: 'cancelDate', key: 'cancelDate', width: 150 },
    { title: 'Approval Date', dataIndex: 'approvalDate', key: 'approvalDate', width: 150 },
    { title: 'Target Date', dataIndex: 'targetdate', key: 'targetdate', width: 150 },
    { title: 'Close Date', dataIndex: 'closedate', key: 'closedate', width: 150 },
        { title: 'Deactivation Date', dataIndex: 'deactivationdate', key: 'deactivationdate', width: 150 },
        { title: 'SID', dataIndex: 'sid', key: 'sid', width: 150 },
        { title: 'Address Originating', dataIndex: 'addressOriginating', key: 'addressOriginating', width: 150 },
        { title: 'SBU Originating', dataIndex: 'sbuOri', key: 'sbuOri', width: 150 },
        { title: 'Address Terminating', dataIndex: 'addressTerminating', key: 'addressTerminating', width: 150 },
        { title: 'SBU Terminating', dataIndex: 'sbuTer', key: 'sbuTer', width: 150 },
        { title: 'Agreement ID', dataIndex: 'agrId', key: 'agrId', width: 150 },
        { title: 'Layanan', dataIndex: 'layanan', key: 'layanan', width: 150 },
        { title: 'Bandwidth', dataIndex: 'bandwidth', key: 'bandwidth', width: 150 },
        { title: 'Qty', dataIndex: 'qty', key: 'qty', width: 150 },
        { title: 'Biaya Sewa', dataIndex: 'biayaSewa', key: 'biayaSewa', width: 150 },
        { title: 'Biaya Instalasi', dataIndex: 'biayaInstalasi', key: 'biayaInstalasi', width: 150 },
        { title: 'Biaya Relokasi', dataIndex: 'biayaRelokasi', key: 'biayaRelokasi', width: 150 },
        { title: 'Activation Actual Date', dataIndex: 'activationactualdate', key: 'activationactualdate', width: 150 },
        { title: 'SBU Owner', dataIndex: 'sbuOwner', key: 'sbuOwner', width: 150 },
        { title: 'Owner', dataIndex: 'owner', key: 'owner', width: 150 },
        { title: 'Project Activation Date', dataIndex: 'paId', key: 'paId', width: 150 },
        { title: 'Project Activation Status', dataIndex: 'paStatus', key: 'paStatus', width: 150 },
        { title: 'PS ID', dataIndex: 'psId', key: 'psId', width: 150 },
        { title: 'Price Lama', dataIndex: 'priceLama', key: 'priceLama', width: 150 },
        { title: 'SID Lama', dataIndex: 'sidLama', key: 'sidLama', width: 150 },
        { title: 'Created On', dataIndex: 'createdon', key: 'createdon', width: 150 },
        { title: 'Activation List Reference', dataIndex: 'alReferenceNo', key: 'alReferenceNo', width: 150 },
        { title: 'Activation List Number', dataIndex: 'alNo', key: 'alNo', width: 150 },
        { title: 'Activation List Status', dataIndex: 'alStatus', key: 'alStatus', width: 150 },
        { title: 'Berita Acara ID', dataIndex: 'baaId', key: 'baaId', width: 150 },
        { title: 'Harga Final', dataIndex: 'hargaFinal', key: 'hargaFinal', width: 150 },
        { title: 'Tahun BA', dataIndex: 'tahunBa', key: 'tahunBa', width: 150 },
        { title: 'Tahun SO', dataIndex: 'targetSo', key: 'targetSo', width: 150 },
        { title: 'Ada AL', dataIndex: 'adaAl', key: 'adaAl', width: 150 },
        { title: 'Berita Acara', dataIndex: 'ba', key: 'ba', width: 150 },
        { title: 'Potensi Berita Acara', dataIndex: 'potensiBa', key: 'potensiBa', width: 150 },
        { title: 'OTC 1', dataIndex: 'otc1', key: 'otc1', width: 150 },
        { title: 'Jumlah Berita Acara', dataIndex: 'jumlahBa', key: 'jumlahBa', width: 150 },
        { title: 'Potensi Progress', dataIndex: 'potensiProgress', key: 'potensiProgress', width: 150 },
        { title: 'OTC 2', dataIndex: 'otc2', key: 'otc2', width: 150 },
        { title: 'Jumlah SO', dataIndex: 'jumlahSo', key: 'jumlahSo', width: 150 },
        { title: 'Group Owner', dataIndex: 'groupOwner', key: 'groupOwner', width: 150 },
        { title: 'Group Layanan', dataIndex: 'groupLayanan', key: 'groupLayanan', width: 150 },
        { title: 'BA Tahun Depan', dataIndex: 'ba2019', key: 'ba2019', width: 150 },
        { title: 'SO Tahun Depan', dataIndex: 'so2019', key: 'so2019', width: 150 },
        { title: 'CO Tahun Depan', dataIndex: 'co2019', key: 'co2019', width: 150 },
        { title: 'Aging', dataIndex: 'aging', key: 'aging', width: 150 }];
    return <Table columns={columns} dataSource={this.state.data} pagination={this.state.pagination} loading={this.state.loading} onChange={this.handleTableChange} scroll={{ x: 900, y: 300 } } rowKey='arNo'/>;
  }
}

