import React from 'react';
import TableActivationRequest from './tableAR';

class ActivationRequestComponent extends React.Component {
    render() {
        return (
            <div>
                <h1 style={{fontSize:20}}>Activation Request</h1><br/>
                <TableActivationRequest />
            </div>
        );
    }
}
 export default ActivationRequestComponent;