
import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './insertgroupowner.css';
import { Button, Modal, Form, Input, Radio } from 'antd';

const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
  class extends React.Component {
    render() {
      const { visible, onCancel, onCreate, form } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="Create a new target sbu"
          okText="Create"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <FormItem label="Group Owner">
              {getFieldDecorator('title', {
                rules: [{ required: true, message: 'Please input the group owner!' }],
              })(
                <Input />
              )}
            </FormItem>
            <FormItem label="Tahun">
              {getFieldDecorator('apa', {
                rules: [{ required: true, message: 'Please input the year!' }],
              })(
                <Input />
              )}
            </FormItem>
            <FormItem label="Target">
              {getFieldDecorator('ini', {
                rules: [{ required: true, message: 'Please input the target!' }],
              })(
                <Input />
              )}
            </FormItem>
          </Form>
        </Modal>
      );
    }
  }
);

export default class Inserttargetsbu extends React.Component {
  state = {
    visible: false,
  };

  showModal = () => {
    this.setState({ visible: true });
  }

  handleCancel = () => {
    this.setState({ visible: false });
  }

  handleCreate = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      console.log('Received values of form: ', values);
      form.resetFields();
      this.setState({ visible: false });
    });
  }

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  }

  render() {
    return (
      <div>
        <Button type="primary" onClick={this.showModal}>New Target Sbu</Button>
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
        />
      </div>
    );
  }
}

          