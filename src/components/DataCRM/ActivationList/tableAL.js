
import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './tableAL.css';
import { Table, Input, Button, Icon, message } from 'antd';
import axios from 'axios';




export default class TableActivationList extends React.Component {
    state = {
        searchText: '',
        data: [],
        pagination: {},
        loading: false,
    };

    handleTableChange = (pagination) => {
        const pager = { ...this.state.pagination };
        pager.current = pagination.current;
        this.setState({
            pagination: pager,
        });

        this.fetch({
            page: pagination.current-1,
            size: pagination.pageSize,
        });
    }

    fetch = (param = {}) => {
        console.log('params:', param);
        this.setState({ loading: true });
        axios.get('http://10.10.10.33:8090/api/v1/view/rawAl/tampilkan', {
            params: {
                size: 50,
                ...param,
            }
        })
            .then((response) => {
                console.log(response.data);
                const pagination = { ...this.state.pagination };
                // Read total count from server
                pagination.total = response.data.totalElements;
                this.setState({
                    loading: false,
                    data: response.data.content,
                    pagination
                });
            })
            .catch(error => {
                this.setState({
                    loading: false,
                    data: []
                });
                message.error(`Status error code ${error.response.code}`);
                console.log(error.response.data);
            });
    }

    componentDidMount() {
        this.fetch();
    }



  handleSearch = (selectedKeys, confirm) => () => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  handleReset = clearFilters => () => {
    clearFilters();
    this.setState({ searchText: '' });
  }

  render() {
    const columns = [
        {title: 'Activation List Number', width: 200, dataIndex: 'alNo', key: 'alNo', fixed: 'left'},
    { title: 'Activation List Status', dataIndex: 'alStatus', key: 'alStatus', width: 200 },
    { title: 'Activation List Type', dataIndex: 'alType', key: 'alType', width: 200 },
    { title: 'Activation Request Number', dataIndex: 'arNo', key: 'arNo', width: 200 },
    { title: 'Activation Request Status', dataIndex: 'arStatus', key: 'arStatus', width: 200 },
    { title: 'Project Activation ID', dataIndex: 'paId', key: 'paId', width: 200 },
    { title: 'Project Activation Status', dataIndex: 'paStatus', key: 'paStatus', width: 200 },
    { title: 'Customer ID', dataIndex: 'custid', key: 'custid', width: 200 },
        { title: 'Customer Name', dataIndex: 'custname', key: 'custname', width: 200 },
        { title: 'Segment', dataIndex: 'segment', key: 'segment', width: 200 },
        { title: 'SID', dataIndex: 'sid', key: 'sid', width: 200 },
        { title: 'Address Originating', dataIndex: 'addressOri', key: 'addressOri', width: 200 },
        { title: 'SBU Originating', dataIndex: 'sbuOri', key: 'sbuOri', width: 200 },
        { title: 'Address Terminating', dataIndex: 'addressTer', key: 'addressTer', width: 200 },
        { title: 'SBU Terminating', dataIndex: 'sbuTer', key: 'sbuTer', width: 200 },
        { title: 'Start Billing Date', dataIndex: 'startbilldate', key: 'startbilldate', width: 200 },
        { title: 'Last Invoice Date', dataIndex: 'lastinvoicedate', key: 'lastinvoicedate', width: 200 },
        { title: 'Deactivation Date', dataIndex: 'deactivationdate', key: 'deactivationdate', width: 200 },
        { title: 'Hold Invoice', dataIndex: 'holdInvoice', key: 'holdInvoice', width: 200 },
        { title: 'Agreement Number', dataIndex: 'agrNo', key: 'agrNo', width: 200 },
        { title: 'Layanan', dataIndex: 'layanan', key: 'layanan', width: 200 },
        { title: 'Bandwidth', dataIndex: 'bandwidth', key: 'bandwidth', width: 200 },
        { title: 'Qty', dataIndex: 'qty', key: 'qty', width: 200 },
        { title: 'Biaya Sewa', dataIndex: 'biayaSewa', key: 'biayaSewa', width: 200 },
        { title: 'Biaya Instalasi', dataIndex: 'biayaInstalasi', key: 'biayaInstalasi', width: 200 },
        { title: 'Biaya Relokasi', dataIndex: 'biayaRelokasi', key: 'biayaRelokasi', width: 200 },
        { title: 'Biaya Sewa Lama', dataIndex: 'biayaSewaLama', key: 'biayaSewaLama', width: 200 },
        { title: 'Activation List Reference', dataIndex: 'alReference', key: 'alReference', width: 200 },
        { title: 'SBU Owner', dataIndex: 'sbuOwner', key: 'sbuOwner', width: 200 },
        { title: 'Owner', dataIndex: 'owner', key: 'owner', width: 200 },
        { title: 'Created On', dataIndex: 'createdon', key: 'createdon', width: 200 },
        { title: 'Tahun Tagih', dataIndex: 'tahunTagih', key: 'tahunTagih', width: 200 },
        { title: 'Bulan Tagih', dataIndex: 'bulanTagih', key: 'bulanTagih', width: 200 },
        { title: 'Tahun Deactivated', dataIndex: 'tahunDeactivated', key: 'tahunDeactivated', width: 200 },
        { title: 'Bulan Deactivated', dataIndex: 'bulanDeactivated', key: 'bulanDeactivated', width: 200 },
        { title: 'Harga Final', dataIndex: 'hargaFinal', key: 'hargaFinal', width: 200 },
        { title: 'CO Active', dataIndex: 'coActive', key: 'coActive', width: 200 },
        { title: 'CO Deactive', dataIndex: 'coDeactive', key: 'coDeactive', width: 200 },
        { title: 'CO Inactive ', dataIndex: 'coInactive', key: 'coInactive', width: 200 },
        { title: 'Carry Over (Tahun Lalu)', dataIndex: 'carryOver2017', key: 'carryOver2017', width: 200 },
        { title: 'NR New', dataIndex: 'nrNew', key: 'nrNew', width: 200 },
        { title: 'NR Upgrade', dataIndex: 'nrUpgrade', key: 'nrUpgrade', width: 200 },
        { title: 'NR Downgrade', dataIndex: 'nrDowngrade', key: 'nrDowngrade', width: 200 },
        { title: 'NR Change Tariff', dataIndex: 'nrChangeTariff', key: 'nrChangeTariff', width: 200 },
        { title: 'NR Relocation', dataIndex: 'nrRelocation', key: 'nrRelocation', width: 200 },
        { title: 'NR OTC', dataIndex: 'nrOtc', key: 'nrOtc', width: 200 },
        { title: 'New Revenue (Tahun Ini)', dataIndex: 'newRevenue', key: 'newRevenue', width: 200 },
        { title: 'Group Owner', dataIndex: 'groupOwner', key: 'groupOwner', width: 200 },
        { title: 'CO Semester 1', dataIndex: 'coJuni', key: 'coJuni', width: 200 },
        { title: 'NR Semester 1', dataIndex: 'nrJuni', key: 'nrJuni', width: 200 },
        { title: 'CO Tahun Depan', dataIndex: 'co2019', key: 'co2019', width: 200 }];

    return <Table columns={columns} dataSource={this.state.data} pagination={this.state.pagination} loading={this.state.loading} onChange={this.handleTableChange} scroll={{ x: 900, y: 300 } } rowKey='alNo' />;
  }
}


