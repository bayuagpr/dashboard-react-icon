import React from 'react';
import TableActivationList from './tableAL';

class ActivationListComponent extends React.Component {
    render() {
        return (
            <div>
                <h1 style={{fontSize:20}}>Activation List</h1><br/>
                <TableActivationList />
            </div>
        );
    }
}
 export default ActivationListComponent;